import XCTest

import ARExtensionsTests

var tests = [XCTestCaseEntry]()
tests += ARExtensionsTests.allTests()
XCTMain(tests)
