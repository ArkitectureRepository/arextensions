Pod::Spec.new do |s|
  s.name             = 'ARExtensions'
  s.version          = '1.0'
  s.summary          = 'Something like summary'

  s.description      = <<-DESC
Something biggest for the description.
                       DESC

  s.homepage         = 'https://gitlab.com/ArkitectureRepository/arextensions.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors          = 'Alvaro Royo'
  s.source           = { :git => 'https://gitlab.com/ArkitectureRepository/arextensions.git' }

  s.ios.deployment_target = '11.0'
  s.swift_version = '5.0'
  
  #s.exclude_files = []
  s.source_files = 'Sources/**/*.swift'
  
  #s.dependency 'OtherDependency', '1.0.0'

end
