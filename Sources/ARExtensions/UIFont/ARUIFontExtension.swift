//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public extension UIFont {
    
    struct CustomFont {
        let name: String
        
        public init(_ name: String) {
            self.name = name
        }
    }
    
    /**
     Load custom font. Make an extension of UIFont.CustomFont and add your project fonts
     - Make an extension of UIFont.CustomFont and add your custom project fonts
     Example:
     
            extension UIFont.CustomFont {
                static let MonserratBold = Self(name: "Monserrat_bold")
            }
     
     If the font name don't existe then take the default system font
     */
    convenience init?(font: CustomFont, size: CGFloat) {
        self.init(name: font.name, size: size)
    }
    
}
