//
//  File.swift
//
//
//  Created by Alvaro Royo on 02/03/2020.
//

import Foundation

public protocol SingleEncodable: Encodable {
    associatedtype type: Encodable
    
    var encode: type? { get }
}

public extension SingleEncodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self.encode)
    }
}

public typealias SingleCodable = Decodable & SingleEncodable
