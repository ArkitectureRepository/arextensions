//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import os

public struct LogType {
    
    fileprivate static var subsystem = "ARLog"
    
    var name: String
    
    public init(_ name: String) {
        self.name = name
    }
    
    fileprivate var type: OSLog {
        return OSLog(subsystem: LogType.subsystem, category: self.name)
    }
    
    fileprivate static var defaultType = OSLog(subsystem: LogType.subsystem, category: "")
}

/**
Print a message on the XCode console and in the Console.app (With a red circle)
 - Make a extension of LogType and define your custom types:
 
       extension LogType {
          static let request = Self(name: "request")
       }
- Parameters:
    - message: Message to show in the print and in the Console.app
    - type: Defined LogType
*/
public func printError(_ message: String, _ type: LogType? = nil) {
    printCustomLog(message, type, .fault)
}

/**
Print a message on the XCode console and in the Console.app (With a yellow circle)
 - Make a extension of LogType and define your custom types:
 
       extension LogType {
          static let request = Self(name: "request")
       }
- Parameters:
    - message: Message to show in the print and in the Console.app
    - type: Defined LogType
*/
public func printWarning(_ message: String, _ type: LogType? = nil) {
    printCustomLog(message, type, .error)
}

/**
Print a message on the XCode console and in the Console.app (With a gray circle)
- Make a extension of LogType and define your custom types:
 
      extension LogType {
         static let request = Self(name: "request")
      }
- Parameters:
    - message: Message to show in the print and in the Console.app
    - type: Defined LogType
*/
public func printDebug(_ message: String, _ type: LogType? = nil) {
    printCustomLog(message, type, .debug)
}

private func printCustomLog(_ message: String, _ log: LogType?, _ type: OSLogType) {
    os_log("%{PUBLIC}s", log: log?.type ?? LogType.defaultType, type: type, message)
}
