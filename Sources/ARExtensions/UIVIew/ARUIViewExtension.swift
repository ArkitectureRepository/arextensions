//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public extension UIView {
    
    ///Set a shadow arround the view that simulate a elevation
    func elevate(elevation: Double = 2.0, color: UIColor = .black, opacity: Double = 0.25) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = abs(CGFloat(elevation))
        self.layer.shadowOpacity = Float(opacity)
    }
    
    ///Modify the view autolayout to hidden the view and get the view space
    var gone: Bool {
        get {
            return (self.constraints.first{ $0.identifier == "w.gone" } != nil)
        }
        set {
            self.isHidden = newValue
            if !translatesAutoresizingMaskIntoConstraints {
                if newValue {
                    let w = self.widthAnchor.constraint(equalToConstant: 0)
                    w.identifier = "w.gone"
                    w.isActive = true
                    let h = self.heightAnchor.constraint(equalToConstant: 0)
                    h.identifier = "h.gone"
                    h.isActive = true
                }else{
                    self.removeConstraints(
                        self.constraints.filter{
                            $0.identifier == "w.gone" || $0.identifier == "h.gone"
                        }
                    )
                }
                self.superview?.layoutIfNeeded()
            }else{
                
            }
        }
    }
    
    //Return view as an image. If your view have an alpha background set ir to isOpaque = false
    func asImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let ctx = UIGraphicsGetCurrentContext() {
            self.layer.render(in: ctx)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            return img
        }
        return nil
    }
    
    ///Round view selected corners.
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    ///Set constraints to the view
    func setConstraintsTo(_ view:UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    private var gradientBackgroundColorsId: String { return "gradientBackgroundColors" }
    //Set background color with gradient colours
    var gradientBackgroundColors: [UIColor] {
        get {
            let layer = self.gradient
            let colors = layer?.colors as? [CGColor]
            return colors?.compactMap{ UIColor(cgColor: $0) } ?? []
        }
        set {
            self.layer.sublayers?.filter{ $0.name == gradientBackgroundColorsId }.forEach{ $0.removeFromSuperlayer() }
            let gradient = CAGradientLayer()
            gradient.name = gradientBackgroundColorsId
            gradient.frame = self.bounds
            gradient.colors = newValue.compactMap{ $0.cgColor }
            self.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    var gradient: CAGradientLayer? { self.layer.sublayers?.first{ $0.name == gradientBackgroundColorsId } as? CAGradientLayer }
    
}

open class UIXibView: UIView {
    
    public var nibName:String? { return String(describing: type(of: self)) }
    
    public var contentView: UIView!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        guard let nibName = self.nibName,
        let view = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? UIView
        else { return }
        self.layoutIfNeeded()
        contentView = view
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        self.addSubview(contentView)
    }
}
