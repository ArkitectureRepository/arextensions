//
//  File.swift
//  
//
//  Created by Alvaro Royo on 30/10/2019.
//

import UIKit

extension UIColor {
    
    convenience public init(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1.0) {
        self.init(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
    
}

