//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import Foundation

public extension Double {
    
    ///Return a formated string from a double.
    var toMoney: String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.roundingMode = .floor
        formatter.numberStyle = .decimal
        return formatter.string(for: self) ?? ""
    }
    
}
