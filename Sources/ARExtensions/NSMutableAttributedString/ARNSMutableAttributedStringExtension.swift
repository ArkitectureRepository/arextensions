//
//  File.swift
//  
//
//  Created by Alvaro Royo on 31/10/2019.
//

import UIKit

extension String {
    
    public var attr: NSMutableAttributedString { NSMutableAttributedString(string: self) }
    
}

extension NSMutableAttributedString {
    
    private var count: Int { self.string.count }
    
    public func add<T>(_ key: NSAttributedString.Key, _ value: T, range: Range<Int>? = nil, string: String? = nil) -> NSMutableAttributedString {
        var range = NSRange(range ?? 0..<self.count)
        if let string = string, let r = self.string.range(of: string) {
            range = NSRange(r, in: self.string)
        }
        self.addAttributes([key: value], range: range)
        return self
    }
    
}
