//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public extension UIImageView {
    
    @IBInspectable
    var alwaysTemplate: Bool {
        set{ self.image = self.image?.withRenderingMode(newValue ? .alwaysTemplate : .alwaysOriginal) }
        get{ return self.image?.renderingMode == .alwaysTemplate }
    }
    
    ///Tint image with gradient
    func tint(with gradientColors: [CGColor]) {
        guard let img = self.image else { return }
        self.image = img.tint(with: gradientColors)
    }
    
}
