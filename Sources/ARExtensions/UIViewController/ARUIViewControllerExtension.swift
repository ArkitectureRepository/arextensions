//
//  File.swift
//  
//
//  Created by Alvaro Royo on 31/10/2019.
//

import UIKit

public extension UIViewController {
    
    ///Show an alert
    func showAlert(_ title: String?, _ message: String?, _ actions: [UIAlertAction], timeOut:TimeInterval? = nil, _ completion: (()->())? = nil) {
        
        if let timeOut = timeOut, timeOut > 0 {
            let timer = Timer.scheduledTimer(withTimeInterval: 0, repeats: false) { _ in
                UIApplication.topViewController.showAlert(title, message, actions, completion)
            }
            timer.tolerance = timeOut
            timer.fire()
            return
        }
        
        let alert = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
        actions.forEach{ alert.addAction($0) }
        
        DispatchQueue.main.async {
            UIApplication.topViewController.present(alert, animated: true, completion: completion)
        }
        
    }
    
    ///Present multiple view controllers (Not recomended)
    func present(_ controllers: [UIViewController], animated: Bool, _ completion: (()->())?) {
        BackgroundThread {
            for vc in controllers {
                let semaphore = DispatchSemaphore(value: 0)
                MainThread {
                    UIApplication.topViewController.present(vc, animated: true) {
                        semaphore.signal()
                    }
                }
                semaphore.wait()
            }
            MainThread {
                completion?()
            }
        }
        
    }
    
}
