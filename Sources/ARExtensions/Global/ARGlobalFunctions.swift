//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import Foundation

public func MainThread(_ completion:@escaping ()->()) {
    DispatchQueue.main.async(execute: completion)
}

public func BackgroundThread(_ completion:@escaping ()->()) {
    DispatchQueue.global().async(execute: completion)
}

public func sleep(seconds: Double) {
    usleep(UInt32(seconds * 1000000))
}
