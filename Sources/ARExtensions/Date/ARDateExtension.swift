//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import Foundation

public extension Date {
    
    ///Transform Date to String. Default format: "dd/MM/yyyy"
    func toString(_ format: String = "dd/MM/yyyy") -> String {
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
    
}
