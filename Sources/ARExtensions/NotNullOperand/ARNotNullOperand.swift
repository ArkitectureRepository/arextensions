//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

postfix operator ¡

public postfix func ¡ <E>(arr:Array<E>?) -> Array<E> {
    return arr == nil ? Array() : arr!
}

public postfix func ¡ (str:String?) -> String {
    return str == nil ? "" : str!
}

public postfix func ¡ (int:Int?) -> Int {
    return int == nil ? 0 : int!
}

public postfix func ¡ (cgfloat:CGFloat?) -> CGFloat {
    return cgfloat == nil ? 0.0 : cgfloat!
}

public postfix func ¡ (bool:Bool?) -> Bool {
    return bool == nil ? false : bool!
}

public postfix func ¡ (font:UIFont?) -> UIFont {
    return font == nil ? UIFont.systemFont(ofSize: 17) : font!
}
