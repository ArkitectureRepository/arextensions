//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import Foundation

public extension String {
    
    ///Get String from Localizable.string file
    var localize: String {
        return NSLocalizedString(self, comment: "")
    }
    
    ///Math String with regular expressions
    func match(_ regularExpression: String) -> Bool {
        return self.range(of: regularExpression, options: .regularExpression) != nil
    }
    
    ///Transform String to Date. Default format: "dd/MM/yyyy"
    func toDate(_ format: String = "dd/MM/yyyy") -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        return df.date(from: self)
    }
    
    ///Prepare text for search removing accents and lowercased
    var searchable: String {
        let replaceChars = ["á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u"]
        var newStr = self.lowercased()
        replaceChars.forEach{ newStr = newStr.replacingOccurrences(of: $0, with: $1) }
        return newStr
    }
    
    ///Return a class from string name. Example: "String".toClass -> String
    var toClass: AnyClass? {
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String
        let cls:AnyClass? = NSClassFromString("\(namespace).\(self)")
        return cls
    }
    
}
