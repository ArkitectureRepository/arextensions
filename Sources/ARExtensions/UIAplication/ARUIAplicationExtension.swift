//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public extension UIApplication {
    
    ///Get root view controller
    static var rootViewController: UIViewController {
        set {
            UIApplication.shared.keyWindow?.rootViewController = newValue
        }
        get {
            return UIApplication.shared.keyWindow!.rootViewController!
        }
    }
    
    ///Get top view controller
    static var topViewController: UIViewController {
        var topController = UIApplication.rootViewController
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
    
}
