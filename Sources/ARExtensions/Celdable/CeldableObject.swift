//
//  CeldableObject.swift
//  bnc10
//
//  Created by Alvaro Royo on 11/12/2018.
//  Copyright © 2018 bnc10. All rights reserved.
//

import Foundation

public protocol CeldableObject {
    
    var cellIdentifier: String { get }
    
}
