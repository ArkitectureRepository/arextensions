//
//  File.swift
//
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public typealias CeldableCell = UITableViewCell & CeldableCellProtocol
public typealias CeldableCollectionCell = UICollectionViewCell & CeldableCellProtocol

public protocol CeldableCellProtocol {
    
    static var identifier: String { get }
    
    func set(obj: Any)
    func setDelegate(delegate: Any)
    
}

public extension CeldableCellProtocol {
    
    static var identifier: String { return "\(self)" }
    
    func setDelegate(delegate: Any) {}
    
}
