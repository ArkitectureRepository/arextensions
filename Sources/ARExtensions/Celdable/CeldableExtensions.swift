//
//  File.swift
//  
//
//  Created by Alvaro Royo on 25/10/2019.
//

import UIKit

public extension UITableView {
    
    func register(_ celdable: CeldableCell.Type) {
        let nib = UINib(nibName: "\(celdable)", bundle: Bundle.main)
        self.register(nib, forCellReuseIdentifier: celdable.identifier)
    }
    
    func dequeueReusableCell(_ celdableObject: CeldableObject, _ indexPath: IndexPath) -> CeldableCell {
        let cell = self.dequeueReusableCell(withIdentifier: celdableObject.cellIdentifier, for: indexPath) as! CeldableCell
        cell.set(obj: celdableObject)
        return cell
    }
    
}

public extension UICollectionView {
    
    func register(_ celdable: CeldableCollectionCell.Type) {
        let nib = UINib(nibName: "\(celdable)", bundle: Bundle.main)
        self.register(nib, forCellWithReuseIdentifier: celdable.identifier)
    }
    
    func dequeueReusableCell(_ celdableObject: CeldableObject, _ indexPath: IndexPath) -> CeldableCollectionCell {
        let cell = self.dequeueReusableCell(withReuseIdentifier: celdableObject.cellIdentifier, for: indexPath) as! CeldableCollectionCell
        cell.set(obj: celdableObject)
        return cell
    }
    
}
