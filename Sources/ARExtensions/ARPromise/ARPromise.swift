//
//  File.swift
//  
//
//  Created by Alvaro Royo on 02/03/2020.
//

import Foundation

public enum ARPromiseStatus:Int{
    case notExecuted
    case executing
    case resultSuccess
    case resultFailure
}

public protocol ARPromiseObj {
    func execute()
}

public class ARPromise<T, E> : ARPromiseObj{
    
    public typealias PromiseSuccessCallback = (T) -> ()
    public typealias PromiseFailureCallback = (E) -> ()
    public typealias PromiseInitParameter = (@escaping PromiseSuccessCallback,@escaping PromiseFailureCallback) -> ()
    
    public var id:String? = nil
    public private(set) var status:ARPromiseStatus = .notExecuted
    
    private var initFunction:PromiseInitParameter? = nil
    private var successCallback:PromiseSuccessCallback? = nil
    private var failureCallback:PromiseFailureCallback? = nil
    
    public typealias VoidPromise = ARPromise<Void,Void>
    private var responsePromise:VoidPromise? = nil
    private var globalStatus:ARPromiseStatus = .resultSuccess
    private var listenPromises:Int = 0
    private var promisesExecuted:Int = 0
    
    private let thenQueue = DispatchQueue(label: "promises.then")
    private var thenPromises:[ARPromiseObj] = []
    
    private init(){}
    
    /**
     Init the promise with it function
     - Parameters:
     - executeFunction: AKPromise function
     - autoExecute: Default **true**. If you set it to **false** the execute function will never execute automatically. We can use this functionallity for call the execute function manually or if the promise depend of other promise (See the **then function** documentation).
     */
    @discardableResult
    public init(_ executeFunction:@escaping PromiseInitParameter, _ autoExecute:Bool = true){
        self.initFunction = executeFunction
        
        if autoExecute {
            self.execute()
        }
    }
    
    /**
     Execute the promise function and it's callbacks
     
     The promises execute this function automatically. If you call this function manually the promise execute twice
     */
    public func execute(){
        self.status = .executing
        DispatchQueue.global().async {
            self.initFunction?({
                self.successCallback?($0)
                self.status = .resultSuccess
                self.sendStatus()
                self.executeThens()
            },{
                self.failureCallback?($0)
                self.status = .resultFailure
                self.sendStatus()
            })
        }
    }
    
    /**
     The promise execute the success callback function when it's ok
     - Parameters:
     - callback: Callback function
     - returns: self
     */
    @discardableResult
    public func success(_ callback:@escaping PromiseSuccessCallback) -> ARPromise {
        self.successCallback = callback
        return self
    }
    
    /**
     The promise execute the failure callback function when fails
     - Parameters:
     - callback: Callback function
     - returns: self
     */
    @discardableResult
    public func failure(_ callback:@escaping PromiseFailureCallback) -> ARPromise {
        self.failureCallback = callback
        return self
    }
    
    /**
     Add a promise that depends of the successful execution of this promise.
     
     For the good execution of this function it's necessary to set as **false** the **autoExecute** init the dependet promise like:
     
     AKPromise<Any,Any>({ success, failure in
     ...
     },false)
     
     - Parameters:
     - promises: Array of dependent promises
     - returns: self
     */
    @discardableResult
    public func then(_ promise:ARPromiseObj) -> ARPromise {
        thenQueue.sync {
            self.thenPromises.append(promise)
        }
        return self
    }
    
    private func sendStatus() {
        if let responsePromise = self.responsePromise {
            responsePromise.receiveStatus(self.status)
        }
    }
    
    private func receiveStatus(_ status:ARPromiseStatus) {
        if status == .resultFailure {
            sendIfFail()
        }
        if status == .resultSuccess {
            if promisesExecuted + 1 >= listenPromises && self.globalStatus == .resultSuccess {
                sendIfSuccess()
            }else{
                promisesExecuted += 1
            }
        }
    }
    
    private func sendIfFail() {
        self.globalStatus = .resultFailure
        let promise = self as! VoidPromise
        promise.initFunction = { _,fail in fail(()) }
        promise.execute()
    }
    
    private func sendIfSuccess() {
        let promise = self as! VoidPromise
        promise.initFunction = { success,_ in success(()) }
        promise.execute()
    }
    
    private func executeThens() {
        thenQueue.sync {
            self.thenPromises.forEach{ $0.execute() }
        }
    }
    
}

//MARK: - Class Functions
extension ARPromise {
    /**
     Execute the array of promises and run the success function of the returned promise if all the promises in the array are success. Else execute the failure function of the returned promise.
     - Parameters:
     - promises: Array of promises
     - returns: A promise thats execute success or failure
     */
    @discardableResult
    public class func all(_ promises: [ARPromise]) -> VoidPromise {
        let responsePromise = VoidPromise()
        responsePromise.listenPromises = promises.count
        
        promises.forEach{
            $0.responsePromise = responsePromise
            if $0.status == .resultSuccess || $0.status == .resultFailure{
                $0.sendStatus()
            }
        }
        
        return responsePromise
    }
}
